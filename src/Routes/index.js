import React from "react";
import { Route, Switch } from "react-router"; // react-router v4
import { ConnectedRouter } from "connected-react-router";
import { history } from "../store";
import { Dashboard,Login,SignUp } from "../App/containers";
export default () => (
  <ConnectedRouter history={history}>
    <Switch>
      <Route exact path="/" component={SignUp} />
      <Route path="/dashboard" component={Dashboard} />
      <Route  path="/login" component={Login} />
      <Route path="/new" render={() => <div>Miss</div>} />
    </Switch>
  </ConnectedRouter>
);

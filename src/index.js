import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import configureStore from "./store";
import RootRoute from "./Routes";
import firebase from 'firebase';
import firebaseConfig from './App/config'

const store = configureStore();

firebase.initializeApp(firebaseConfig);

ReactDOM.render(
  <Provider store={store}>
    <RootRoute />
  </Provider>,
  document.getElementById("root")
);

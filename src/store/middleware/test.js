import { test } from "../actions";
import { push } from "connected-react-router";

export const requestTest = payload => {
  return (dispatch, getState) => {
    dispatch(push("/new"));
    dispatch(test(payload));
  };
};

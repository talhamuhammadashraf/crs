import React, { Component } from 'react';
import {connect} from 'react-redux'

class SignUp extends Component {
    constructor(){
        super();
    }
    handleChange = (event)=>{
        this.setState({[event.target.name]:event.target.value})
    }
    handleSubmit = (event) =>{
        event.preventDefault();
        const {name,email,password} = this.state
        const user = {name,email,password}
        console.log(user,"user")
    }
    render() {
        return (
            <div>
                SignUp
                <form onSubmit={this.handleSubmit}>
                    <input placeholder="name" name="name" onChange={this.handleChange}/>
                    <input placeholder="email" name="email" onChange={this.handleChange}/>
                    <input placeholder="password" name="password" onChange={this.handleChange}/>
                    <button onClick={this.handleSubmit}>Submit</button>
                </form>
            </div>
        )
    }
}
const mapStateToProps = (state) =>({state});
const mapDispatchToProps = (dispatch) =>({});
export default connect(mapStateToProps,mapDispatchToProps)(SignUp)
import React, { Component } from "react";
import { connect } from "react-redux";
import { requestTest } from "../../../store/middleware";
class Dashboard extends Component {
  constructor() {
    super();
    this.state = {};
  }
  componentDidMount() {}
  render() {
    return (
      <div>
        <button onClick={this.props.click}>Click</button>
        Dashboard
      </div>
    );
  }
}

const mapStateToProps = state => ({ state });
const mapDispatchToProps = dispatch => ({
  click: param => dispatch(requestTest(param))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
